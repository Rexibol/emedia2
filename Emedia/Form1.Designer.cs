﻿namespace Emedia
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.buttonReadFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileBmpDialog = new System.Windows.Forms.OpenFileDialog();
            this.open = new System.Windows.Forms.Button();
            this.filepath = new System.Windows.Forms.Label();
            this.info = new System.Windows.Forms.Label();
            this.chart = new System.Windows.Forms.Label();
            this.chartFFT = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.fileProperties = new System.Windows.Forms.TextBox();
            this.textBoxfilepath = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.saveFileBpm = new System.Windows.Forms.Button();
            this.decryptButton = new System.Windows.Forms.Button();
            this.encryptButton = new System.Windows.Forms.Button();
            this.textBoxBmp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonReadBmp = new System.Windows.Forms.Button();
            this.buttonOpenBmp = new System.Windows.Forms.Button();
            this.textBoxUrlBmp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartFFT)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonReadFile
            // 
            this.buttonReadFile.Enabled = false;
            this.buttonReadFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonReadFile.Location = new System.Drawing.Point(18, 74);
            this.buttonReadFile.Name = "buttonReadFile";
            this.buttonReadFile.Size = new System.Drawing.Size(398, 36);
            this.buttonReadFile.TabIndex = 0;
            this.buttonReadFile.Text = "Wczytaj";
            this.buttonReadFile.UseVisualStyleBackColor = true;
            this.buttonReadFile.Click += new System.EventHandler(this.buttonReadFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openFileBmpDialog
            // 
            this.openFileBmpDialog.FileName = "openFileDialogBmp";
            // 
            // open
            // 
            this.open.Location = new System.Drawing.Point(338, 43);
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(78, 25);
            this.open.TabIndex = 1;
            this.open.Text = "Otwórz";
            this.open.UseVisualStyleBackColor = true;
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // filepath
            // 
            this.filepath.AutoSize = true;
            this.filepath.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.filepath.Location = new System.Drawing.Point(15, 13);
            this.filepath.Name = "filepath";
            this.filepath.Size = new System.Drawing.Size(126, 17);
            this.filepath.TabIndex = 2;
            this.filepath.Text = "Ścieżka do pliku";
            // 
            // info
            // 
            this.info.AutoSize = true;
            this.info.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.info.Location = new System.Drawing.Point(15, 133);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(107, 17);
            this.info.TabIndex = 3;
            this.info.Text = "Atrybuty pliku";
            // 
            // chart
            // 
            this.chart.AutoSize = true;
            this.chart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.chart.Location = new System.Drawing.Point(15, 278);
            this.chart.Name = "chart";
            this.chart.Size = new System.Drawing.Size(110, 17);
            this.chart.TabIndex = 4;
            this.chart.Text = "Wykres widma";
            // 
            // chartFFT
            // 
            chartArea2.Name = "ChartArea1";
            this.chartFFT.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartFFT.Legends.Add(legend2);
            this.chartFFT.Location = new System.Drawing.Point(18, 308);
            this.chartFFT.Name = "chartFFT";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "FFT";
            this.chartFFT.Series.Add(series2);
            this.chartFFT.Size = new System.Drawing.Size(746, 406);
            this.chartFFT.TabIndex = 5;
            this.chartFFT.Text = "chartFFT";
            // 
            // fileProperties
            // 
            this.fileProperties.Location = new System.Drawing.Point(18, 160);
            this.fileProperties.Multiline = true;
            this.fileProperties.Name = "fileProperties";
            this.fileProperties.Size = new System.Drawing.Size(398, 96);
            this.fileProperties.TabIndex = 6;
            // 
            // textBoxfilepath
            // 
            this.textBoxfilepath.Location = new System.Drawing.Point(18, 44);
            this.textBoxfilepath.Name = "textBoxfilepath";
            this.textBoxfilepath.Size = new System.Drawing.Size(314, 22);
            this.textBoxfilepath.TabIndex = 7;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(890, 770);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBoxfilepath);
            this.tabPage1.Controls.Add(this.chartFFT);
            this.tabPage1.Controls.Add(this.fileProperties);
            this.tabPage1.Controls.Add(this.chart);
            this.tabPage1.Controls.Add(this.filepath);
            this.tabPage1.Controls.Add(this.open);
            this.tabPage1.Controls.Add(this.buttonReadFile);
            this.tabPage1.Controls.Add(this.info);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(882, 741);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Plik .WAV";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.saveFileBpm);
            this.tabPage2.Controls.Add(this.decryptButton);
            this.tabPage2.Controls.Add(this.encryptButton);
            this.tabPage2.Controls.Add(this.textBoxBmp);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.buttonReadBmp);
            this.tabPage2.Controls.Add(this.buttonOpenBmp);
            this.tabPage2.Controls.Add(this.textBoxUrlBmp);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(882, 741);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Plik BMP";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // saveFileBpm
            // 
            this.saveFileBpm.Enabled = false;
            this.saveFileBpm.Location = new System.Drawing.Point(452, 133);
            this.saveFileBpm.Name = "saveFileBpm";
            this.saveFileBpm.Size = new System.Drawing.Size(188, 67);
            this.saveFileBpm.TabIndex = 15;
            this.saveFileBpm.Text = "Zapisz plik";
            this.saveFileBpm.UseVisualStyleBackColor = true;
            this.saveFileBpm.Click += new System.EventHandler(this.saveFileBpm_Click);
            // 
            // decryptButton
            // 
            this.decryptButton.Enabled = false;
            this.decryptButton.Location = new System.Drawing.Point(646, 43);
            this.decryptButton.Name = "decryptButton";
            this.decryptButton.Size = new System.Drawing.Size(188, 67);
            this.decryptButton.TabIndex = 14;
            this.decryptButton.Text = "Deszyrfuj";
            this.decryptButton.UseVisualStyleBackColor = true;
            this.decryptButton.Click += new System.EventHandler(this.buttonDecrypt_Click);
            // 
            // encryptButton
            // 
            this.encryptButton.Enabled = false;
            this.encryptButton.Location = new System.Drawing.Point(452, 43);
            this.encryptButton.Name = "encryptButton";
            this.encryptButton.Size = new System.Drawing.Size(188, 67);
            this.encryptButton.TabIndex = 13;
            this.encryptButton.TabStop = false;
            this.encryptButton.Text = "Szyfruj";
            this.encryptButton.UseVisualStyleBackColor = true;
            this.encryptButton.Click += new System.EventHandler(this.buttonEncrypt_Click);
            // 
            // textBoxBmp
            // 
            this.textBoxBmp.Location = new System.Drawing.Point(18, 160);
            this.textBoxBmp.Multiline = true;
            this.textBoxBmp.Name = "textBoxBmp";
            this.textBoxBmp.Size = new System.Drawing.Size(398, 192);
            this.textBoxBmp.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(15, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Atrybuty pliku";
            // 
            // buttonReadBmp
            // 
            this.buttonReadBmp.Enabled = false;
            this.buttonReadBmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonReadBmp.Location = new System.Drawing.Point(18, 74);
            this.buttonReadBmp.Name = "buttonReadBmp";
            this.buttonReadBmp.Size = new System.Drawing.Size(398, 36);
            this.buttonReadBmp.TabIndex = 10;
            this.buttonReadBmp.Text = "Wczytaj";
            this.buttonReadBmp.UseVisualStyleBackColor = true;
            this.buttonReadBmp.Click += new System.EventHandler(this.buttonReadBmp_Click);
            // 
            // buttonOpenBmp
            // 
            this.buttonOpenBmp.Location = new System.Drawing.Point(338, 43);
            this.buttonOpenBmp.Name = "buttonOpenBmp";
            this.buttonOpenBmp.Size = new System.Drawing.Size(78, 25);
            this.buttonOpenBmp.TabIndex = 9;
            this.buttonOpenBmp.Text = "Otwórz";
            this.buttonOpenBmp.UseVisualStyleBackColor = true;
            this.buttonOpenBmp.Click += new System.EventHandler(this.buttonOpenBmp_Click);
            // 
            // textBoxUrlBmp
            // 
            this.textBoxUrlBmp.Location = new System.Drawing.Point(18, 44);
            this.textBoxUrlBmp.Name = "textBoxUrlBmp";
            this.textBoxUrlBmp.Size = new System.Drawing.Size(314, 22);
            this.textBoxUrlBmp.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(15, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ścieżka do pliku";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 765);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.chartFFT)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonReadFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileBmpDialog;
        private System.Windows.Forms.Button open;
        private System.Windows.Forms.Label filepath;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.Label chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartFFT;
        private System.Windows.Forms.TextBox fileProperties;
        private System.Windows.Forms.TextBox textBoxfilepath;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBoxBmp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonReadBmp;
        private System.Windows.Forms.Button buttonOpenBmp;
        private System.Windows.Forms.TextBox textBoxUrlBmp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button decryptButton;
        private System.Windows.Forms.Button encryptButton;
        private System.Windows.Forms.Button saveFileBpm;
    }
}

