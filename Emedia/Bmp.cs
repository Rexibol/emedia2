﻿using System;
using System.IO;

namespace Emedia
{
    class Bmp
    {
        //zmienne
        private const int HEADER_SIZE = 54;

        public byte[] readHeader(byte[] file)
        {
            byte[] header = new byte[HEADER_SIZE];
            for (int i = 0; i < HEADER_SIZE; i++)
            {
                header[i] = file[i];
            }
            return header;
        }

        public byte[] readFile(String filePath)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
            {
                long fileSize = new System.IO.FileInfo(filePath).Length;
//                Console.WriteLine(fileSize);
                byte[] file = new byte[fileSize - HEADER_SIZE];
                file = reader.ReadBytes((int)fileSize);
                return file;
            }
        }

        public byte[] readFileWithoutHeader(byte[] file)
        {
            byte[] fileWithoutHeader = new byte[file.Length - HEADER_SIZE];
            Console.WriteLine("readFileWithoutHeader");
            for(int i = HEADER_SIZE; i < file.Length; i++)
            {
                fileWithoutHeader[i-HEADER_SIZE]= file[i];
            }
            return fileWithoutHeader;
        }

        public void saveFile(String filePath, byte[] fileWithoutHeader, byte[] header)
        {
            byte[] file = new byte[fileWithoutHeader.Length + header.Length];
            for (int i = 0; i < header.Length; i++)
                file[i] = header[i];

            for (int i = header.Length; i < fileWithoutHeader.Length + header.Length; i++)
                file[i] = fileWithoutHeader[i - header.Length];

            File.WriteAllBytes(filePath, file);
        }

        public void displayFileWithoutHeader(byte[] fileWithoutHeader)
        {
            Console.WriteLine("displayFileWithoutHeader");
            for(int i = 0; i < fileWithoutHeader.Length; i++)
            {
                Console.WriteLine(fileWithoutHeader[i]);
            }
        }
        public void displayFileSize(byte[] file)
        {
            Console.WriteLine(file.Length);
        }
        public void displayHeader(byte[] header)
        {
            Console.WriteLine("displayHeader");
            for (int i = 0; i < HEADER_SIZE; i++)
            {
                Console.WriteLine(header[i]);
            }
        }
        public long getFileHeight(byte[] header)
        {
            long height = header[25] * 256 + header[24] * 256 + header[23] * 256 + header[22];
            return height;
        }

        public long getFileWidth(byte[] header)
        {
            long width = header[21] * 256 + header[20] * 256 + header[19] * 256 + header[18];
            return width;
        }

        public long getFileSize(byte[] header)
        {
            //http://stackoverflow.com/questions/12336994/bitmap-file-header-size
            long width = getFileWidth(header);
            long height = getFileHeight(header);
            long size = width * height * 3 + HEADER_SIZE + (width % 4) * height;
            return size;
        }
        public long getBitsPerPixel(byte[] header)
        {
            return header[29] * 256 + header[28];
        }

        public string getCompressionType(byte[] header)
        {
            //https://www.fastgraph.com/help/bmp_header_format.html
            //compression type (0=none, 1=RLE-8, 2=RLE-4)
            switch (header[30])
            {
                case 0:
                    return "Brak";

                case 1:
                    return "RLE-8";

                case 2:
                    return "RLE-4";

                default:
                    return "Nie rozpoznano";
            }
        }
    }
}
