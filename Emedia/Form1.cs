﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Math;


namespace Emedia
{
    public partial class Form1 : Form
    {
        Wav wav = new Wav();
        Bmp bmp = new Bmp();
        RSA rsa = new RSA();
        private byte[] file;
        private byte[] header;
        private byte[] fileWithoutHeader;
        private byte[] fileWithoutHeaderEncrypted;
        RSACryptoServiceProvider rsaCryptoServiceProvider = new RSACryptoServiceProvider();

        public Form1()
        {
            InitializeComponent();
            openFileDialog1.Filter = "wav files (*.wav)|*.wav";
            openFileDialog1.Title = "Wybierz plik audio";
            openFileBmpDialog.Filter = "bmp files (*.bmp)|*.bmp";
            openFileBmpDialog.Title = "Wybierz plik bmp";
            buttonReadFile.Enabled = textBoxfilepath.Text.Length != 0;
        }

        private void open_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxfilepath.Text = openFileDialog1.FileName;
                buttonReadFile.Enabled = true;
            }
        }

        private void buttonReadFile_Click(object sender, EventArgs e)
        {
            string fileName = textBoxfilepath.Text;
            wav.readWavFile(fileName);
            fileProperties.Text = "Liczba kanałów: " + wav.NumChannels()
                + "\r\nCzęstotliwość próbkowania: " + wav.SampleRate()
                + "\r\nRozdzielczość: " + wav.BitsPerSample() + " bitów";

            chartFFT.Series["FFT"].Points.Clear();

            const int dataNO = 1024;
            PrintFft(fileName, dataNO);
        }

        public void PrintFft(string fileName, int dataNO) //dataNo have to be 2^n
        {
            int[] sample = wav.readWavContent(fileName, dataNO);
            AForge.Math.Complex[] complexData = new Complex[dataNO];
            for (int i = 0; i < dataNO; i++)
            {
                complexData[i].Re = sample[i];
            }

            int[] frq = new int[dataNO / 2];
            int fp = wav.SampleRate();
            int dFp = (fp / 2 + 1) / (dataNO / 2);
            frq[0] = dFp;
            for (int i = 1; i < dataNO / 2; i++)
            {
                frq[i] = frq[i - 1] + dFp;
            }
            AForge.Math.FourierTransform.FFT(complexData, FourierTransform.Direction.Forward);
            for (int i = 1; i < complexData.Length / 2; ++i)
            {
                chartFFT.Series["FFT"].Points.AddXY(frq[i], complexData[i].Magnitude);
            }
        }

        private void buttonOpenBmp_Click(object sender, EventArgs e)
        {
            if (openFileBmpDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxUrlBmp.Text = openFileBmpDialog.FileName;
                buttonReadBmp.Enabled = true;
            }
        }
        private void buttonReadBmp_Click(object sender, EventArgs e)
        {
            string filePath = textBoxUrlBmp.Text;
            file = bmp.readFile(filePath);

            header = bmp.readHeader(file);
            fileWithoutHeader = bmp.readFileWithoutHeader(file);

            textBoxBmp.Text = "";
            textBoxBmp.AppendText("Wysokość: " + bmp.getFileHeight(header));
            textBoxBmp.AppendText(Environment.NewLine);
            textBoxBmp.AppendText("Szerokosc: " + bmp.getFileWidth(header));
            textBoxBmp.AppendText(Environment.NewLine);
            textBoxBmp.AppendText("Rozmiar pliku: " + bmp.getFileSize(header) + " bajtów");
            textBoxBmp.AppendText(Environment.NewLine);
            textBoxBmp.AppendText("Ilość bitów na jeden pixel: " + bmp.getBitsPerPixel(header));
            textBoxBmp.AppendText(Environment.NewLine);
            textBoxBmp.AppendText("Kompresja: " + bmp.getCompressionType(header));
            textBoxBmp.AppendText(Environment.NewLine);

            // bmp.displayFileWithoutHeader(fileWithoutHeader);
            encryptButton.Enabled = true;
        }

        private void saveFileBpm_Click(object sender, EventArgs e)
        {
            Console.WriteLine("saveFileBpm_Click");
            string filePath = "C:\\Users\\Konrad\\Documents\\Visual Studio 2015\\Projects\\colorminiNew.bmp";
            bmp.saveFile(filePath, fileWithoutHeader, header);
        }

        private void buttonEncrypt_Click(object sender, EventArgs e)
        {
            Console.WriteLine("buttonEncrypt_Click-Szyfrujemy");
            fileWithoutHeaderEncrypted = rsa.RSAEncrypt(fileWithoutHeader, rsaCryptoServiceProvider.ExportParameters(false), false);
            fileWithoutHeader = fileWithoutHeaderEncrypted;
            saveFileBpm.Enabled = true;
            decryptButton.Enabled = true;
        }

        private void buttonDecrypt_Click(object sender, EventArgs e)
        {
            Console.WriteLine("buttonDecyrpt_Click -Deszyrfujemy");
            byte[] decryptedFile = rsa.RSADecrypt(fileWithoutHeader, rsaCryptoServiceProvider.ExportParameters(true), false);
            fileWithoutHeader = decryptedFile;
        }
    }
}
