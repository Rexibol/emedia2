﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emedia
{
    class Wav
    {
        private byte[] _header;
        private const int HeaderSize = 44;

        public int[] readWavContent(string fileName, int sample)
        {
            if (File.Exists(fileName))
            {
                byte[] temp;
                using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open)))
                {
                    _header = reader.ReadBytes(HeaderSize);
                    temp = new byte[this.BitsPerSample() == 16 ? 2*sample:sample];
                    temp = this.BitsPerSample() == 16 ? reader.ReadBytes(2*sample) : reader.ReadBytes(sample);
                }
                int[] samples = new int[sample];
                if (this.BitsPerSample() == 16)
                {
                    for (int i = 0; i < sample; i++)
                    {
                        samples[i] = (temp[2*i]) + (temp[2 * i + 1] << 8) + 32768;
                    }
                    return samples;
                }
                else
                {
                    for (int i = 0; i < sample; i++)
                    {
                        samples[i] = temp[i];
                    }
                    return samples;
                }
            }
            return null;
        }

        public void readWavFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open)))
                {
                    _header = reader.ReadBytes(HeaderSize);
                }
            }
        }

        public int NumChannels()
        {
            return _header[22] + (_header[23] << 8);
        }

        public Int32 SampleRate()
        {
            return (((Int32)_header[27])<<32 )+ (((Int32)_header[26]) <<16) + (((Int32)_header[25]) <<8) + (((Int32)_header[24]));
        }

        public int BitsPerSample()
        {
            return _header[34] + (_header[35] << 8);
        }

    }
}
